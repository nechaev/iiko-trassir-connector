import java.sql.DriverManager

import com.typesafe.config.ConfigFactory

import scala.xml.XML

/**
 * Created by sgl on 05.07.14.
 */
object Terminals {
  def main(args: Array[String]) {
    val conf = ConfigFactory.load().getConfig("iikoDB")
    val jdbcUrl = conf.getString("jdbcUrl")
    val user = conf.getString("userName")
    val password = conf.getString("password")
    val conn = DriverManager.getConnection(jdbcUrl, user, password)
    val rs = conn.prepareStatement("select id, xml from entity where type='Terminal'").executeQuery()
    while (rs.next()) {
      val terminalXml = XML.loadString(rs.getString("xml"))
      println(s"Terminal ID: ${rs.getString("id")}, name: ${(terminalXml \\ "friendlyName").text}, computer: ${(terminalXml \\ "computerName").text}")
    }
    rs.close()
    conn.close()
  }
}
