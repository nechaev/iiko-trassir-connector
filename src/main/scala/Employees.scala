/**
 * Created by sgl on 08.06.14.
 */
class Employees(val lastTimestamp: Long, val empMap: Map[String, String]) {
  def this(empList: Seq[(String, String)]) = this(System.currentTimeMillis(), Map(empList: _*))
  def apply(id: String) = empMap(id)
}
