import java.io.StringWriter
import java.net.InetSocketAddress

import akka.actor.{Stash, ActorRef, Actor}
import akka.io.Udp.Send
import akka.io.{Udp, Tcp, IO}
import akka.io.Tcp._
import akka.util.ByteString
import com.typesafe.scalalogging.slf4j.LazyLogging
import scala.concurrent.duration._
import scala.xml.{XML, NodeSeq}

/**
 * Created by sgl on 13.07.14.
 */
class TrassirTerminal(protocol: String, addr: InetSocketAddress,
                      encoding: String, excluded: Set[String], sendOnCheckClose: Boolean)
  extends Actor with Stash with LazyLogging {
  import context.system
  import context.dispatcher

  var pendingChecks = Map.empty[String, List[NodeSeq]]

  def receiveTcp: Receive = {
    case ConnectTerminal => connectTcp()
    case CommandFailed(_: Connect) => {
      logger.error("Connect failed, retrying")
      connectTcp()
    }
    case _: ConnectionClosed => {
      logger.info("Connection closed, reconnecting")
      connectTcp()
    }
    case _: Connected => {
      logger.info(s"Trassir terminal ${self.path.elements.last} connected to $addr")
      val conn = sender()
      conn ! Register(self)
      unstashAll()
      context.become(receiveConnectedTcp(conn), discardOld = false)
    }
    case _:NodeSeq => stash()
  }
  def connectTcp() {
    IO(Tcp) ! Connect(addr)
  }

  def receiveUdp: Receive = {
    case ConnectTerminal => IO(Udp) ! Udp.SimpleSender
    case Udp.SimpleSenderReady => {
      unstashAll()
      context.become(receiveConnectedUdp(sender()))
    }
    case _: NodeSeq => stash()
  }

  def receiveConnectedTcp(conn: ActorRef): Receive = {
    case msg: NodeSeq => send(msg, conn ! Write(_))
    case CheckPendingTimeout(opId) => pendingChecks -= opId
    case _: CommandFailed => {
      logger.error("Connection failed")
      conn ! Close
      connectTcp()
      context.unbecome()
    }
    case _: ConnectionClosed => {
      logger.info("Connection closed, reconnecting")
      connectTcp()
      context.unbecome()
    }
  }

  def receiveConnectedUdp(sender: ActorRef): Receive = {
    case msg: NodeSeq => send(msg, sender ! Send(_, addr))
    case CheckPendingTimeout(opId) => pendingChecks -= opId
  }

  def send(msg: NodeSeq, sender: ByteString => Unit) {
    val eventType = (msg \ "event_type").text
    if (!(excluded contains eventType)) {
      val opId = (msg \ "operation_id").text
      if (sendOnCheckClose && opId.startsWith("check")) {
        val events = msg :: pendingChecks.getOrElse(opId, {
            context.system.scheduler.scheduleOnce(30.minutes, self, CheckPendingTimeout(opId))
            Nil
          })
        if (eventType.equals("pos_receipt_close")) {
          events.reverse.foreach(pendingMsg => sender(encode(pendingMsg)))
          pendingChecks -= opId
        } else pendingChecks += opId -> events
      } else sender(encode(msg)) 
    }
  }
  
  def encode(msg: NodeSeq) = {
    val writer = new StringWriter()
    //XML.write(writer, msg.head, term.encoding, true, null) //Trassir cannot parse single quotes in declaration
    writer.write("<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>\n")
    XML.write(writer, msg.head, "", xmlDecl = false, doctype = null)
    val payload = writer.toString
    logger.trace(s"Sending message $payload to $addr")
    ByteString(payload.getBytes(encoding))
  }

  def receive = protocol match {
    case "tcp" => receiveTcp
    case "udp" => receiveUdp
  }

  override def preStart {
    self ! ConnectTerminal
    logger.info(s"Connecting to trassir terminal ${self.path.elements.last} on ${addr.toString} by $protocol}")
  }

}

case object ConnectTerminal
case class CheckPendingTimeout(opId: String)
