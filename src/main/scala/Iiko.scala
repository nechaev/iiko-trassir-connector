import akka.actor.{Props, Status, ActorSystem, Actor}
import akka.pattern.pipe
import akka.util.Timeout
import com.typesafe.config.Config
import com.typesafe.scalalogging.slf4j.LazyLogging
import java.security.MessageDigest
import java.time.format.DateTimeFormatter
import java.time.LocalDateTime
import java.time.temporal.{ChronoUnit, TemporalAmount, TemporalUnit}
import java.util.concurrent.TimeUnit
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.math.BigDecimal.RoundingMode
import scala.xml.NodeSeq
import spray.http._
import spray.client.pipelining._

/**
 * Created by sgl on 02.06.14.
 */
class Iiko(conf: Config) extends Actor with LazyLogging {
  var tokenOpt: Option[String] = None
  var nextRevision:Int = 0
  val baseUrl = s"http://${conf.getString("host")}:${conf.getString("port")}/resto/api/"
  val authUpdateTimeout = conf.getDuration("authUpdateTimeout", TimeUnit.MILLISECONDS) millis
  val loadOnStart = conf.getDuration("loadOnStart", TimeUnit.MILLISECONDS)
  val login = conf.getString("login")
  val pass = hash(conf.getString("password"))
  var employees = new Employees(0, Map())
  val trassir = context.actorOf(Props(classOf[Trassir], conf), "trassir")

  import context.dispatcher
  implicit val timeout: Timeout = 15 seconds
  def authResponseReader(response: HttpResponse) = {
    if (response.status.isSuccess) Authenticated(response.entity.asString)
    else throw new RuntimeException("Authentication failure")
  }

  def setUnicode(resp: HttpResponse) = resp.entity match {
    case e@HttpEntity.NonEmpty(contentType, data) =>
      resp.copy(entity = e.copy(contentType = contentType.copy(definedCharset = Some(HttpCharsets.`UTF-8`))))
    case _ => resp
  }

  val authPipeline = sendReceive ~> authResponseReader
  val apiRequestPipeline = sendReceive ~> setUnicode ~> unmarshal[NodeSeq]

  def receive = {
    case Authenticated(token) => {
      logger.debug(s"Authenticated with token $token")
      tokenOpt = Some(token)
      self ! UpdateEmployees
    }
    case UpdateEmployees => withToken { token =>
      apiRequestPipeline(Get(baseUrl + s"employees?key=$token")) foreach { employeesXml =>
        val employeeNodes = employeesXml \\ "employee"
        logger.debug(s"Received ${employeeNodes.length} employees")
        employees = new Employees(employeeNodes.map(employeeXml => (
          (employeeXml \ "id").text,
          (employeeXml \ "lastName").text + " " + (employeeXml \ "firstName").text + " " + (employeeXml \ "middleName").text
          )))
        logger.trace(employees.empMap.toString)
        self ! PollEvents
      }
    }
    case PollEvents => withToken { token =>
      (if (nextRevision == 0) {
        val fromTime = DateTimeFormatter.ISO_DATE_TIME.format(LocalDateTime.now().minus(loadOnStart, ChronoUnit.MILLIS))
        logger.trace("Initial event request: from $fromTime")
        apiRequestPipeline(Get(baseUrl + s"events?from_time=$fromTime&key=$token"))
      } else {
        logger.trace(s"Requesting events from revision $nextRevision")
        apiRequestPipeline(Get(baseUrl + s"events?from_rev=$nextRevision&key=$token"))
      }).map({ events =>
        nextRevision = (events \\ "revision").text.toInt + 1
        val eventNodes = events \\ "event"
        logger.debug(s"Received ${eventNodes.length} events")
        eventNodes foreach parseEvent
      }).onFailure({
        case _ => {
          logger.warn("Request failed, resetting auth info")
          tokenOpt = None
          self ! PollEvents
        }
      })
      if (System.currentTimeMillis() - employees.lastTimestamp > authUpdateTimeout.toMillis) tokenOpt = None
    }
  }
  
  def withToken(block: String => Unit) {
    tokenOpt match {
      case None => {
        val url = baseUrl + s"auth?login=$login&pass=$pass"
        logger.debug(s"Requesting authentication token from $url")
        authPipeline(Get(url)) pipeTo self
      }
      case Some(token) => block(token)
    }
  }

  def hash(str: String) = {
    val d = MessageDigest.getInstance("SHA-1")
    d.reset()
    d.update(str.getBytes("ASCII"))
    d.digest().map("%02x" format _).mkString
  }
  val dateFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy")
  val timeFormat = DateTimeFormatter.ofPattern("HH:mm:ss")
  def parseEvent(eventXml: NodeSeq) {
    val id = (eventXml \ "id").text
    val eventType = (eventXml \ "type").text
    logger.trace(s"Parsing event $id of type $eventType")
    val attrs = Map((eventXml \ "attribute").map(attrXml => (attrXml \ "name").text -> (attrXml \ "value").text):_*)
    val terminalId = attrs.getOrElse("terminal", "")
    val eventId = attrs.get("orderNum") match {
      case Some(orderNum) => "check-" + BigDecimal(orderNum).setScale(0).toString()
      case None => id
    }
    val date = DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse((eventXml \ "date").text)
    def trassirEvent(trassirEventType:String, position: String = "0", name: String = "", qty: Int = 0, price: String = "0") =
      TrassirMessage(terminalId, <transaction>
        <event_type>{trassirEventType}</event_type>
        <operation_id>{eventId}</operation_id>
        <cashier>{attrs.get("user").flatMap(employees.empMap.get).getOrElse("")}</cashier>
        <date>{dateFormat.format(date)}</date>
        <time>{timeFormat.format(date)}</time>
        <position>{position}</position>
        <name>{name}</name>
        <quantity>{qty}</quantity>
        <code></code>
        <weight>0</weight>
        <price>{price}</price>
      </transaction>)
    def sendOrderPaidEvent {
      trassir ! trassirEvent("pos_final_result", name = eventId, price = attrs.getOrElse("orderSumAfterDiscount", "0"))
      attrs.get("sumCash").foreach(sum =>
        if (!sum.equals("0E-9")) trassir ! trassirEvent("pos_cash_payment", price = BigDecimal(sum).setScale(2, RoundingMode.HALF_UP).toString())
      )
      attrs.get("sumCard").foreach(sum =>
        if (!sum.equals("0E-9")) trassir ! trassirEvent("pos_credit_card_payment", price = BigDecimal(sum).setScale(2, RoundingMode.HALF_UP).toString())
      )
      trassir ! trassirEvent("pos_receipt_close")
    }
    eventType match {
      case "cardAuthorization" => trassir ! trassirEvent("pos_cashier_registration")
      case "persSessionOpened" => trassir ! trassirEvent("pos_work_session_start")
      case "xReportPrinted" => trassir ! trassirEvent("pos_x_report")
      case "frontLogout" => trassir ! trassirEvent("pos_exit_from_the_system")
      case "orderOpened" => {
        trassir ! trassirEvent("pos_new_receipt_sell")
        trassir ! trassirEvent("pos_receipt_number", name = eventId)
      }
      case "orderPrechequed" => trassir ! trassirEvent("pos_preliminary_result", price = BigDecimal(attrs("prechequeSum")).setScale(2).toString(), name = eventId)
      case "orderPaid" => sendOrderPaidEvent
      case "orderPaidCredit" => sendOrderPaidEvent
      case "orderPaidNoCash" => sendOrderPaidEvent
      case "addItemToOrder" => trassir ! trassirEvent("pos_position_add",
        position = BigDecimal(attrs("rowCount")).setScale(0, RoundingMode.FLOOR).toString(),
        name = attrs("dishes"),
        qty = 1,
        price = BigDecimal(attrs("sum")).setScale(2).toString()
      )
      case "deletedNewItems" => trassir ! trassirEvent("pos_position_cancel",
        position = BigDecimal(attrs("rowCount")).setScale(0, RoundingMode.FLOOR).toString(),
        name = attrs("dishes"),
        qty = 1,
        price = BigDecimal(attrs("sum")).setScale(2).toString()
      )
      case _ =>
    }
  }

}

case class Authenticated(token: String)
case object UpdateEmployees
case object PollEvents
