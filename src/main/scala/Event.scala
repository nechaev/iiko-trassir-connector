import java.time.Instant

/**
 * Created by sgl on 01.06.14.
 */
abstract class Event(val time: Instant, val department: Int) {

}
