import akka.actor.{Actor, Props}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.slf4j.LazyLogging
import java.util.concurrent.TimeUnit
import scala.concurrent.duration._
import scala.language.postfixOps

/**
 * Created by sgl on 31.05.14.
 */
class Main extends Actor with LazyLogging {
  val conf = ConfigFactory.load()
  val iiko = context.actorOf(Props(classOf[Iiko], conf))
  val pollInterval = conf.getDuration("pollInterval", TimeUnit.MILLISECONDS)
  import context.dispatcher
  def receive = {
    case DoUpdate => iiko ! PollEvents
  }

  override def preStart() {
    context.system.scheduler.schedule(Duration.Zero, pollInterval millis, self, DoUpdate)
  }
}

case object DoUpdate

