import akka.actor.{Props, ActorRef, Actor}
import akka.io.{IO, Tcp}
import akka.util.ByteString
import com.typesafe.config.Config
import com.typesafe.scalalogging.slf4j.LazyLogging
import java.io.StringWriter
import java.net.InetSocketAddress
import scala.xml.{XML, NodeSeq}
import scala.collection.JavaConversions._

/**
 * Created by sgl on 01.06.14.
 */
class Trassir(conf: Config) extends Actor with LazyLogging {

  override def preStart() {
    val trassirConf = conf.getObject("trassir")
    for (termConf <- trassirConf.keys.map(trassirConf.toConfig.getConfig)) {
      context.actorOf(Props(classOf[TrassirTerminal],
      termConf.getString("protocol"),
        new InetSocketAddress(termConf.getString("host"), termConf.getInt("port")),
        if (termConf.hasPath("encoding")) termConf.getString("encoding") else "windows-1251",
        if (termConf.hasPath("excluded")) termConf.getStringList("excluded").toSet else Set.empty,
        if (termConf.hasPath("sendOnCheckClose")) termConf.getBoolean("sendOnCheckClose") else false
      ), termConf.getString("id"))
    }
  }

  def receive = {
    case TrassirMessage(id, msg) => {
      context.child(id) match {
        case Some(terminal) => terminal ! msg
        case None => logger.warn(s"Unknown terminal $id")
      }
    }
  }

}

case class TrassirMessage(terminal: String, message: NodeSeq)
