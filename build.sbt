name := "iiko-trassir-connector"

scalaVersion := "2.11.1"

scalacOptions in Compile += "-feature"

resolvers ++= Seq(
    "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
    "spray repo" at "http://repo.spray.io"
)

libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % "2.3.3",
    "io.spray" %% "spray-client" % "1.3.1-20140423",
    "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2"
)